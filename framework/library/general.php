<?php

   /*

      tapaculo: web application framework
      Copyright (c) 2017 Christian Brenner. All rights reserved.
      Copyright (c) 2017 praclear. All rights reserved.

      Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
      following conditions are met:

      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
         following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
         following disclaimer in the documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
      WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
      USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   */

   namespace Tapaculo\Framework\Library;

   use Tapaculo\Framework\Specification;

   class General implements Specification\General {

      private $array_key;
      private $array_value;
      private $position_equalsign;
      private $uri_requested;
      private $uri_query;
      private $uri_query_array;
      private $uri_query_array_rectified;

      public function __construct() {

         // variables
         $this->uri_requested  = $_SERVER['REQUEST_URI'];

      }

      public function compareStrings(string $string1, string $string2) {

         return (strcmp($string1, $string2) === 0 ? true : false);

      }

      public function returnUriPath() {

         return trim(parse_url($this->uri_requested, PHP_URL_PATH), '/');

      }

      public function returnUriQueryArray() {

         // variables
         $this->uri_query                 = parse_url($this->uri_requested, PHP_URL_QUERY);
         $this->uri_query_array_rectified = array();

         // variables with dependencies
         $this->uri_query_array           = explode('&', $this->uri_query);

         foreach ($this->uri_query_array as $this->array_key => $this->array_value) {

            // variables
            $this->position_equalsign           = stripos($this->array_value, '=');

            // variables with dependencies
            $this->uri_query_array_rectified[]  = ['ident'  => substr($this->array_value, 0, $this->position_equalsign),
                                                   'value'  => substr($this->array_value, ++$this->position_equalsign, strlen($this->array_value))];

         }

         return $this->uri_query_array_rectified;

      }

   }

?>