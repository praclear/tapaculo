<?php

   /*

      tapaculo: web application framework
      Copyright (c) 2017 Christian Brenner. All rights reserved.
      Copyright (c) 2017 praclear. All rights reserved.

      Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
      following conditions are met:

      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
         following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
         following disclaimer in the documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
      WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
      USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   */

   namespace Tapaculo\Framework\Library;

   use Tapaculo\Framework\Specification;

   class Filesystem implements Specification\Filesystem {

      public function makeDirectory(string $directory_path, int $directory_permissions, bool $option_recursive) {

         return (is_dir($directory_path) != true ? mkdir($directory_path, $directory_permissions, $option_recursive) : false);

      }

      public function returnContentsOfDirectory(string $directory_path) {

         return (is_dir($directory_path) == true ? array_values(array_diff(scandir($directory_path), ['.', '..'])) : false);

      }

      public function returnContentsOfFile(string $file_path) {

         return (file_exists($file_path) != false ? file($file_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) : false);

      }

      public function writeDataToFile(string $file_path, string $data) {

         return (file_put_contents($file_path, $data."\r\n", LOCK_EX | FILE_APPEND) != false ? true : false);

      }

   }

?>