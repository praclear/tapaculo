<?php

   /*

      tapaculo: web application framework
      Copyright (c) 2017 Christian Brenner. All rights reserved.
      Copyright (c) 2017 praclear. All rights reserved.

      Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
      following conditions are met:

      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
         following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
         following disclaimer in the documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
      WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
      USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   */

   namespace Tapaculo\Framework\Library;

   use \Exception;
   use \PDO;
   use \PDOException;
   use Tapaculo\Framework\Specification;

   class Mysql implements Specification\Mysql {

      private $connection_dsn;
      private $connection_parameter;
      private $database_name;
      private $database_password;
      private $database_username;
      private $server_address;
      private $server_port;

      public function establishDbmsConnection(array $connection_data) {

         // variables
         $this->database_name          = $connection_data['database_name'];
         $this->database_password      = $connection_data['database_password'];
         $this->database_username      = $connection_data['database_username'];
         $this->server_address         = $connection_data['server_address'];
         $this->server_port            = $connection_data['server_port'];

         // variables with dependencies
         $this->connection_dsn         = 'mysql:dbname='.$this->database_name.';host='.$this->server_address.';port='.$this->server_port;
         $this->connection_parameter   = [PDO::ATTR_EMULATE_PREPARES => constant('SETTING_DATABASE_EMULATION_PREPARES'),
                                          PDO::ATTR_PERSISTENT       => constant('SETTING_DATABASE_CONNECTION_PERSISTENCE'),
                                          PDO::ATTR_TIMEOUT          => constant('SETTING_DATABASE_CONNECTION_TIMEOUT'),
                                          PDO::MYSQL_ATTR_COMPRESS   => constant('SETTING_DATABASE_CONNECTION_COMPRESSION')];

         try {

            return new PDO($this->connection_dsn, $this->database_username, $this->database_password, $this->connection_parameter);

         } catch (PDOException $dbms_exception) {

            throw new Exception($dbms_exception->getMessage());

         }

      }

   }

?>