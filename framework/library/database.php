<?php

   /*

      tapaculo: web application framework
      Copyright (c) 2017 Christian Brenner. All rights reserved.
      Copyright (c) 2017 praclear. All rights reserved.

      Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
      following conditions are met:

      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
         following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
         following disclaimer in the documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
      WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
      USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   */

   namespace Tapaculo\Framework\Library;

   use \PDO;
   use Tapaculo\Framework\Specification;

   class Database implements Specification\Database {

      private $array_key;
      private $array_value;
      private $connection_data;
      private $database_connection;
      private $database_name;
      private $database_password;
      private $database_username;
      private $dataset_rectified;
      private $dbms_name;
      private $dbms_server_address;
      private $dbms_server_port;
      private $keyword_search;
      private $library_classname;
      private $library_dbms;
      private $result_exec;
      private $result_prepare;
      private $result_set;
      private $sql_statement;

      private function establishDatabaseConnection() {

         // variables
         $this->database_name       = constant('SETTING_DATABASE_NAME');
         $this->database_password   = constant('SETTING_DATABASE_PASSWORD');
         $this->database_username   = constant('SETTING_DATABASE_USERNAME');
         $this->dbms_name           = constant('SETTING_DBMS_NAME');
         $this->dbms_server_address = constant('SETTING_DBMS_SERVER_ADDRESS');
         $this->dbms_server_port    = constant('SETTING_DBMS_SERVER_PORT');

         // variables with dependencies
         $this->connection_data     = ['database_name'      => $this->database_name,
                                       'database_password'  => $this->database_password,
                                       'database_username'  => $this->database_username,
                                       'server_address'     => $this->dbms_server_address,
                                       'server_port'        => $this->dbms_server_port];

         // invoke dbms library
         $this->library_classname   = __NAMESPACE__.'\\'.ucfirst($this->dbms_name);
         $this->library_dbms        = new $this->library_classname;

         return $this->library_dbms->establishDbmsConnection($this->connection_data);

      }

      private function executePreparedSqlStatement(string $sql_statement, array $sql_placeholder) {

         // variables
         $this->database_connection = $this->establishDatabaseConnection();

         // variables with dependencies
         $this->result_prepare      = $this->database_connection->prepare($sql_statement);

         return ($this->result_prepare->execute($sql_placeholder) == true ? $this->result_prepare : false);

      }

      public function countAllDatasets(string $table_name, string $table_column) {

         // variables
         $this->sql_statement = 'SELECT COUNT('.$table_column.') FROM '.$table_name;

         // variables with dependencies
         $this->result_set    = $this->executePreparedSqlStatement($this->sql_statement, array());

         return $this->result_set->fetchColumn();

      }

      public function countAllSpecificDatasets(string $table_name, string $table_column, array $parameter_search) {

         // variables
         $this->keyword_search   = array();

         foreach ($parameter_search as $this->array_key => $this->array_value) {

            $this->keyword_search[] = $this->array_key.'=?';

         }

         // variables with dependencies
         $this->sql_statement    = 'SELECT COUNT('.$table_column.') FROM '.$table_name.' WHERE '.implode(' AND ', $this->keyword_search);
         $this->result_set       = $this->executePreparedSqlStatement($this->sql_statement, array_values($parameter_search));

         return $this->result_set->fetchColumn();


      }

      public function deleteDataset(string $table_name, array $parameter_search) {

         // variables
         $this->keyword_search   = array();

         foreach ($parameter_search as $this->array_key => $this->array_value) {

            $this->keyword_search[] = $this->array_key.'=?';

         }

         // variables with dependencies
         $this->sql_statement    = 'DELETE FROM '.$table_name.' WHERE '.implode(' AND ', $this->keyword_search);
         $this->result_exec      = $this->executePreparedSqlStatement($this->sql_statement, array_values($parameter_search));

         return ($this->result_exec != false ? true : false);

      }

      public function fetchAllResultSets(string $table_name, array $table_columns, int $result_offset, int $result_limit, string $order_column, string $order_sort) {

         // variables
         $this->sql_statement = 'SELECT '.implode(',', $table_columns).' FROM '.$table_name.' ORDER BY '.$order_column.' '.strtoupper($order_sort).' LIMIT '.$result_limit.' OFFSET '.$result_offset;

         // variables with dependencies
         $this->result_set    = $this->executePreparedSqlStatement($this->sql_statement, array());

         return $this->result_set->fetchAll(PDO::FETCH_ASSOC);

      }

      public function fetchAllSpecificResultSets(string $table_name, array $table_columns, array $parameter_search, int $result_offset, int $result_limit, string $order_column, string $order_sort) {

         // variables
         $this->keyword_search   = array();

         foreach ($parameter_search as $this->array_key => $this->array_value) {

            $this->keyword_search[] = $this->array_key.'=?';

         }

         // variables with dependencies
         $this->sql_statement    = $this->sql_statement = 'SELECT '.implode(',', $table_columns).' FROM '.$table_name.' WHERE '.implode(' AND ', $this->keyword_search).' ORDER BY '.$order_column.' '.strtoupper($order_sort).' LIMIT '.$result_limit.' OFFSET '.$result_offset;
         $this->result_set       = $this->executePreparedSqlStatement($this->sql_statement, array());

         return $this->result_set->fetchAll(PDO::FETCH_ASSOC);

      }

      public function fetchSingleReturnSet(string $table_name, array $table_columns, array $parameter_search) {

         // variables
         $this->keyword_search   = array();

         foreach ($parameter_search as $this->array_key => $this->array_value) {

            $this->keyword_search[] = $this->array_key.'=?';

         }

         // variables with dependencies
         $this->sql_statement    = 'SELECT '.implode(',', $table_columns).' FROM '.$table_name.' WHERE '.implode(' AND ', $this->keyword_search);
         $this->result_set       = $this->executePreparedSqlStatement($this->sql_statement, array_values($parameter_search));

         return $this->result_set->fetch(PDO::FETCH_ASSOC);

      }

      public function insertDatasetIntoTable(string $table_name, array $dataset) {

         // variables
         $this->sql_statement = 'INSERT INTO '.$table_name.' ('.(implode(',', array_keys($dataset))).') VALUES ('.(implode(',', array_values($dataset))).')';

         // variables with dependencies
         $this->result_exec   = $this->executePreparedSqlStatement($this->sql_statement, []);

         return ($this->result_exec != false ? true : false);

      }

      public function updateDataset(string $table_name, array $dataset, array $parameter_search) {

         // variables
         $this->dataset_rectified   = array();
         $this->keyword_search      = array();

         foreach ($dataset as $this->array_key => $this->array_value) {

            $this->dataset_rectified[] = $this->array_key.'=?';

         }

         foreach ($parameter_search as $this->array_key => $this->array_value) {

            $this->keyword_search[] = $this->array_key.'=?';

         }

         // variables with dependencies
         $this->sql_statement = 'UPDATE '.$table_name.' SET '.implode(',', $this->dataset_rectified).' WHERE '.implode(' AND ', $this->keyword_search);
         $this->result_exec   = $this->executePreparedSqlStatement($this->sql_statement, array_merge(array_values($dataset), array_values($parameter_search)));

         return ($this->result_exec != false ? true : false);

      }

   }

?>