<?php

   /*

      tapaculo: web application framework
      Copyright (c) 2017 Christian Brenner. All rights reserved.
      Copyright (c) 2017 praclear. All rights reserved.

      Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
      following conditions are met:

      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
         following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
         following disclaimer in the documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
      WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
      USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   */

   namespace Tapaculo\Framework\Core;

   // settings: date and time
   define('SETTING_DATEANDTIME_FORMAT',                  DATE_RFC850);
   define('SETTING_DATEANDTIME_TIMEZONE',                'UTC');

   // settings: database
   define('SETTING_DATABASE_CONNECTION_COMPRESSION',     true);
   define('SETTING_DATABASE_CONNECTION_PERSISTENCE',     true);
   define('SETTING_DATABASE_CONNECTION_TIMEOUT',         15);
   define('SETTING_DATABASE_EMULATION_PREPARES',         true);
   define('SETTING_DATABASE_LIMIT_FETCH',                50);
   define('SETTING_DATABASE_NAME',                       '');
   define('SETTING_DATABASE_ORDER_SORT',                 'ASC');
   define('SETTING_DATABASE_PASSWORD',                   '');
   define('SETTING_DATABASE_USERNAME',                   '');

   // settings: database management system
   define('SETTING_DBMS_NAME',                           'mysql');
   define('SETTING_DBMS_SERVER_ADDRESS',                 '');
   define('SETTING_DBMS_SERVER_PORT',                    3306);

   // settings: security
   define('SETTING_SECURITY_ALGORITHM_CIPHER',           'AES-256-CFB');
   define('SETTING_SECURITY_ALGORITHM_HASH',             'sha512');
   define('SETTING_SECURITY_PERMISSIONS_DIRECTORY',      0740);
   define('SETTING_SECURITY_TOKEN',                      '');

?>