<?php

   /*

      tapaculo: web application framework
      Copyright (c) 2017 Christian Brenner. All rights reserved.
      Copyright (c) 2017 praclear. All rights reserved.

      Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
      following conditions are met:

      1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
         following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
         following disclaimer in the documentation and/or other materials provided with the distribution.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
      SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
      SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
      WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
      USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   */

   namespace Tapaculo\Framework\Core;

   define('ENVIRONMENT_BASELINE',                                 realpath(dirname(basename($_SERVER['REQUEST_URI']))));

   // directory: framework
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK',                      ENVIRONMENT_BASELINE.'/framework/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_API',                  ENVIRONMENT_DIRECTORY_FRAMEWORK.'api/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_CORE',                 ENVIRONMENT_DIRECTORY_FRAMEWORK.'core/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_EXTENSION',            ENVIRONMENT_DIRECTORY_FRAMEWORK.'extension/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_LIBRARY',              ENVIRONMENT_DIRECTORY_FRAMEWORK.'library/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_MESSAGE',              ENVIRONMENT_DIRECTORY_FRAMEWORK.'message/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_SPECIFICATION',        ENVIRONMENT_DIRECTORY_FRAMEWORK.'specification/');

   // directory: framework/message
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_MESSAGE_ERROR',        ENVIRONMENT_DIRECTORY_FRAMEWORK_MESSAGE.'error/');
   define('ENVIRONMENT_DIRECTORY_FRAMEWORK_MESSAGE_EXCEPTION',    ENVIRONMENT_DIRECTORY_FRAMEWORK_MESSAGE.'exception/');

   // directory: log
   define('ENVIRONMENT_DIRECTORY_LOG',                            '/var/log/tapaculo/');

   // directory: stage
   define('ENVIRONMENT_DIRECTORY_STAGE',                          ENVIRONMENT_BASELINE.'/stage/');
   define('ENVIRONMENT_DIRECTORY_STAGE_FRONTEND',                 ENVIRONMENT_DIRECTORY_STAGE.'frontend/');
   define('ENVIRONMENT_DIRECTORY_STAGE_OPERATION',                ENVIRONMENT_DIRECTORY_STAGE.'operation/');
   define('ENVIRONMENT_DIRECTORY_STAGE_TEMPLATE',                 ENVIRONMENT_DIRECTORY_STAGE.'template/');

?>